function changeFlag(language){
    const FLAG = document.getElementById("atualFlag");


    if(language == "#en-us") FLAG.src = flagchange.us;
    else FLAG.src = flagchange.br
}


// function changeText(language){
//     const paragrafos = document.querySelectorAll(".paragrafo");
//     {
//         paragrafos.forEach(paragrafo => {
//             paragrafo.textContent = language.paragrafo
//         })

//         hi.textContent = language.hi
//         subtitle.textContent = language.subtitle
//         phrase.textContent = language.phrase
//     }    
// }

function changeText(language){
    const CHANGE = document.querySelectorAll('.change');

    CHANGE.forEach(item => {
        const ID = item.id;
        item.innerHTML = language[ID]
    })
}

function changeMenu(language){
    let lng;
    const HOME = document.querySelectorAll('.homeMenu');
    const SERVICES = document.querySelectorAll('.servicesMenu');
    const WEB = document.querySelectorAll('.devWebMenu');
    const APPS = document.querySelectorAll('.appsMenu');
    const MARKETING = document.querySelectorAll('.marketingMenu');
    const ABOUT = document.querySelectorAll('.aboutMenu');
    const CONTACT = document.querySelectorAll('.contactMenu')

    if(language == "#en-us") lng = menuLanguages.en
    else lng = menuLanguages.pt

    HOME.forEach(item => {
        item.textContent = lng.home;
    });
    SERVICES.forEach(item => {
        item.textContent = lng.services.title;
    });
    WEB.forEach(item => {
        item.textContent = lng.services.web;
    });
    APPS.forEach(item => {
        item.textContent = lng.services.apps;
    });
    MARKETING.forEach(item => {
        item.textContent = lng.services.marketing;
    });
    ABOUT.forEach(item => {
        item.textContent = lng.about;
    });
    CONTACT.forEach(item => {
        item.innerHTML = lng.contact;
    });

}