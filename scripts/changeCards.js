const CARDS = document.querySelectorAll('.card');   

CARDS.forEach(item => {
    item.addEventListener('mouseover', () => {
        item.classList.add('flip');
        item.classList.remove('flipped');
    })

    item.addEventListener('mouseout', () => {
        item.classList.add('flipped');
        setTimeout(() =>{
            if(item.classList.contains('flipped')){
                item.classList.remove('flip');
            }

        }, 1000);
    })
})