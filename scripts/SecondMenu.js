const secondMenu = document.querySelector('#second-nav-container');

let scrollPosition = 0;
let ticking = true;

document.addEventListener('scroll', e => {
    scrollPosition = window.scrollY;

    if(scrollPosition >=106){
        ticking = false;
    }else{
        ticking = true;
    }

    showHideSecondMenu(ticking);
})

function showHideSecondMenu(ticking){
    if(!ticking){
        secondMenu.classList.add('show-second');
        secondMenu.classList.remove('hide-second');
    }else{
        secondMenu.classList.add('hide-second');
        secondMenu.classList.remove('show-second');
    }
}