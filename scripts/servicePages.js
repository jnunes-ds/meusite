const CARDBUTTONS = document.querySelectorAll('.card-button');
const SERVICES = document.querySelector('#services');
const imagePlace = './images/img/pages/';

let language = 'pt-br';

if(window.location.hash){
    if(window.location.hash == "#en-us") language = "en-us"
    else language = 'pt-br';
}

CARDBUTTONS.forEach(button => {
    button.addEventListener('click', () => {
        let cardFront = button.parentElement;
        let theCard = cardFront.parentElement;
        
        createNewServicePage(theCard, language);   
    }) 
});



    
function createNewServicePage(about, language){
    
    
    let quit = document.createElement('div');
    let content = document.createElement('div');
    let photoContainer = document.createElement('div');
    let textContainer = document.createElement('div');
    let image = document.createElement('img');
    let { photoAdress, textContent, buttonContent } = createContent(about, language);
    let botao = document.createElement('button');
    let servicePage = document.createElement('div');
    
    botao.classList.add('service-page-button');
    servicePage.id = 'the-page';
    servicePage.classList.add('service-page');
    content.classList.add('service-page-content');
    photoContainer.classList.add('service-page-image');
    textContainer.classList.add('service-page-text-container');
    quit.classList.add('quit-page');
    
    botao.textContent = `${buttonContent}`;
    quit.textContent = 'X';
    image.src = photoAdress;
    textContainer.innerHTML = textContent;
    
    photoContainer.appendChild(image);
    content.appendChild(photoContainer);
    textContainer.appendChild(botao);
    content.appendChild(textContainer);
    servicePage.appendChild(quit);
    servicePage.appendChild(content);
    SERVICES.appendChild(servicePage);
    
    
    
    quit.addEventListener('click', () => {
        removePageService(SERVICES, servicePage);
    })
    botao.addEventListener('click', () => {
        suaveScrool('#contact');
        removePageService(SERVICES, servicePage);
        
    }) 
}

function createContent(about, language){
    // Choosing a Language
    let choosingLanguage;
    
    if(language == 'en-us'){
        choosingLanguage = PAGES.EnUS
    }else{
        choosingLanguage = PAGES.PtBr
    }

    // Choosing a Card
    let choosingCard;

    if(about.id == 'devWeb-Card' || about.id == 'devWeb-mobile-Card'){
        choosingCard = choosingLanguage.devWeb
    }else if(about.id == 'appMobile-Card' || about.id == 'appMobile-mobile-Card'){
        choosingCard = choosingLanguage.appMobile
    }else if(about.id == 'appDesktop-Card' || about.id == 'appDesktop-mobile-Card'){
        choosingCard = choosingLanguage.appDesktop
    }else if(about.id == 'marketPlace-Card' || about.id == 'marketPlace-mobile-Card'){
        choosingCard = choosingLanguage.marketPlace
    }else if(about.id == 'digitalMarketing-Card' || about.id == 'digitalMarketing-mobile-Card'){
        choosingCard = choosingLanguage.digitalMarketing
    }else return '#ERRO! PÁGINA NÃO EXISTE';

    let text = choosingCard.text;
    let photoAdress = choosingCard.photo;
    let textContent = `${text.title} ${text.p1} ${text.p2} ${text.p3} ${text.p4} ${text.p5} ${text.p6}`;
    let buttonContent = `${text.button}`;

    return {photoAdress, textContent, buttonContent};
}

function aClickService(about){

    let About = document.querySelector(`#${about}`);
    let page = document.querySelector('#the-page');
    let services = document.querySelector('#services');


    removePageService(services, page)

    setTimeout(createNewServicePage(About, language), 3000);
    
}

function removePageService(container, content){
    container.removeChild(content);
}

const PAGES = {
    EnUS: {
        devWeb: {
            photo: `${imagePlace}devWeb.jpg`,
            text: {
                title: '<h1>Web Development</h1>',
                p1: '<br><p>The website is a mandatory tool for more than 99% of business and this should be obvious.</p>',
                p2: '<br>'+
                    '<p>'+
                        'Wether to expand the number of costumers by facilitating Google’s search engines'+
                        '(<a href="#" onClick="aClickService(`digitalMarketing-Card`)"><strong>See also on Digital Marketing</strong></a>), or simply '+
                        'to keep a positioning of your company so that those who are already customers can learn more, the website'+
                        'is the most professional, elegant and effective way to do it.'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                    'Show the face of your company and create portfolios to showcase your services and products, or sell them'+
                    'directly on your website (<a href="#" onClick="aClickService(`marketPlace-Card`)"><strong>Click here to learn more about E-commerce</strong></a>),'+
                    'the possibilities are endless when working with this wonderful tool with decades of consolidation.'+
                    '</p>',
                p4: '',
                p5: '',
                p6: '',
                button: 'Request a quote here'
            }
        },
        appMobile: {
            photo: `${imagePlace}appMobile.jpg`,
            text: {
                title: '<h1>Why an Application?</h1>',
                p1: '<br><p>The computer model most used in the world today is the smartphone</p>',
                p2: '<br>'+
                    '<p>'+
                        'In this way, any self-respecting company (whether large or small), knows that mastering this tecnology is not longer a differentiator,'+
                        'but an obligation'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                        'Whether to have control of your business in the palm of you hand, to create a direct channel between your company and your customers, or even '+
                        'to get away from charging from a more traditional application, there is a mobile solution for you.'+
                    '</p>',
                p4: '<br>'+
                    '<p>'+
                        'Whether to have control of your business in the palm of you hand, to create a direct channel between your company and your customers, or even '+
                        'to get away from charging from a more traditional application, there is a mobile solution for you.'+
                    '</p>',
                p5: '<br>'+
                    '<p>'+
                        '<b>OBS.:</b> Mobile applications can be used on a system in conjunction with <a href="#" onClick="aClickService(`appDesktop-Card`)"><strong>Desktop Applications</strong></a>.'+
                    '</p>',
                p6: '',
                button: 'Request your app'
            }
        },
        appDesktop:{
            photo: `${imagePlace}appDesktop.jpg`,
            text: {
                title: '<h1>Desktop Apps</h1>',
                p1: '<br>'+
                    '<p>'+
                        'Whether to automate tasks or to have greater control over your company’s processes, desktop applications are excellent.'+
                    '</p>',
                p2: '<br>'+
                    '<p>'+
                        'These programs (or applications) are tailor-made by Brazilian Harpy in order to facilitate or, if possible, automate activities that, '+
                        'although essential for the functioning of your business,  are repetitive and end up exhausting your precious working time.'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                        'And believe me, having an application that does simple tasks like calculating the correct amount to be charged for a service based on working time '+
                        'or that automatically fowards e-mails directed to certain groups of customers makes total difference in the productivity. '+
                        'of your company.'+
                    '</p>',
                p4: '',
                p5: '',
                p6: '',
                button: 'Request your app'
            }
        },
        marketPlace: {
            photo: `${imagePlace}marketPlace.jpg`,
            text: {
                title: '<h1>E-commerce</h1>',
                p1: '<br>'+
                    '<p>'+
                        'Having an online showcase where customers can see the desired products and their prices is wonderful. But making these products accessible '+
                        'to be bought from the confort of you home is certainly much better.'+
                    '</p>',
                p2: '<br>'+
                    '<p>'+
                        'And it’s not just about traditional stores that we’re talking, whether it’s to facilitate the hiring of a service, for exemple, or to sell '+
                        'digital products, E-commerce is the ideal answer.'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                    'This service however, does not stand alone and is always associated with three other services that Brazilian Harpy also offers: '+
                    '<a href="#" onClick="aClickService(`devWeb-Card`)" ><strong>Web Development</strong></a>, <a href="#" onClick="aClickService(`appMobile-Card`)" ><strong>Mobile applications</strong></a> and '+
                    '<a href="#" onClick="aClickService(`appDesktop-Card`)" ><strong>Desktop Applications</strong></a>.'+
                    '</p>',
                p4: '<br>'+
                    '<p>'+
                        'This is due to the fact that the virtual store is nothing more than a database with all its products and values, but it is through the website'+
                        'or application that the customers will actualy have access to this information, being able to finally hire/buy your service/Product.'+
                    '</p>',
                p5: '',
                p6: '',
                button: 'Request your own online store'
            }
        },
        digitalMarketing: {
            photo: `${imagePlace}digitalMarketing.jpg`,
            text: {
                title: '<h1>Digital Marketing</h1>',
                p1: '<br>'+
                    '<p>'+
                        'No matter how good your business is, if it fails to reach new customers, it will close its doors.'+
                    '</p>',
                p2: '<br>'+
                    '<p>'+
                        'It is with this in mind that Brazilian Harpy understands that as important as helping you to have an excellent service, is helping you to'+
                        'have a greater reach so that new people get to know your service.'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                        'This way we provide you a Digital Marketing service, boosting your website in Google searches as well as company’s'+
                        'social media so that your brand and work can reach more people. This is the perfect service for anyone looking to increase'+
                        'the radius of impact that the company produces in the world.'+
                    '</p>',
                p4: '',
                p5: '',
                p6: '',
                button: 'Expand your business now'
            }
        }
    },
    PtBr: {
        devWeb:{
            photo: `${imagePlace}devWeb.jpg`,
            text: {
                title: '<h1>Desenvolvimento Web</h1>',
                p1: '<br>'+
                    '<p>'+
                        'O website é uma ferramenta obrigatória para mais de 99% dos negócios e isso deveria ser óbvio.'+
                    '</p>',
                p2: '<br>'+
                    '<p>'+
                        'Seja para expandir o número de clientes por facilitar os mecanismos de busca do Google '+
                        '(<a href="#" onClick="aClickService(`digitalMarketing-Card`);"><strong>veja também sobre Marketing Digital</strong></a>), ou simplesmente '+
                        'para manter um posicionamento da sua empresa para que quem já é cliente possa conhecer mais, o Website '+
                        'é a foma mais profissional, elegante e eficaz de fazê-lo.'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                        'Mostre a cara da sua empresa e crie portfólios para expor os seis serviços e produtos, ou venda-os '+
                        'diretamente no seu site (<a href="#" onClick="aClickService(`marketPlace-Card`)" ><strong>Clique aqui e veja mais sobre E-commerce</strong></a>), '+
                        'as possibilidades são infinitas quando se trabalha com essa maravilhosa ferramenta com décadas de consolidação. '+
                    '</p>',
                p4: '',
                p5: '',
                p6: '',
                button: 'Solicite aqui um orçamento'
            }
        },
        appMobile: {
            photo: `${imagePlace}appMobile.jpg`,
            text: {
                title: '<h1>Por que um aplicativo?</h1>',
                p1: '<br>'+
                    '<p>'+
                        'O modelo de computador mais utilizado no mundo hoje é o smartphone.'+
                    '</p>',
                p2: '<br>'+
                    '<p>'+
                        'Dessa forma, qualquer empresa que se preze (Seja de grande ou pequeno porte), sabe que dominar essa tecnologia não é mais um diferencial, '+
                        'mas uma obrigação.'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                        'É pensando nisso que a Brazilian Harpy tem para você um serviço voltado especificamente para o de desenvolvimento de aplicativos Mobile.'+
                    '</p>',
                p4: '<br>'+
                    '<p>'+
                        'Seja para ter o controle do seu negócio na palma da sua mão, para criar um canal direto entre a sua empresa e os seus clientes, ou mesmo '+
                        'para fugir da tarifação de algum aplicativo mais tradicional, há uma solução Mobile para você.'+
                    '</p>',
                p5: '<br>'+
                    '<p>'+
                        '<b>OBS.:</b> Aplicativos Mobile podem ser utilizados num sistema em conjunto com <a href="#" onClick="aClickService(`appDesktop-Card`)"><strong>aplicativos Desktop</strong></a>.'+
                    '</p>',
                p6: '',
                button: 'Solicite o seu app'
            }
        },
        appDesktop:{
            photo: `${imagePlace}appDesktop.jpg`,
            text: {
                title: '<h1>Aplicatios Desktop</h1>',
                p1: '<br>'+
                    '<p>'+
                        'Seja para automatizar tarefas ou ter maior controle sobre os processos da sua empresa, as aplicações desktop são excelentes.'+
                    '</p>',
                p2: '<br>'+
                    '<p>'+
                        'Esses programas (ou aplicativos) são feitos sob medida pela Brazilian Harpy afim de facilitar ou, se possível, automatizar atividades que '+
                        'apesar de essenciais para o funcionamento do seu negócio, são repetitivas e acabam por esgotar o seu precioso tempo de trabalho.'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                        'E acredite, ter um aplicativo que faz tarefas simples como calcular o valor correto a ser cobrado por um  serviço baseado no tempo de '+
                        'trabalho ou que, de forma automática, encaminha e-mails direcionados para determinados grupos de clientes faz total diferença na produtividade '+
                        'da sua empresa.'+
                    '</p>',
                p4: '',
                p5: '',
                p6: '',
                button: 'Solicite o seu app'
            }
        },
        marketPlace: {
            photo: `${imagePlace}marketPlace.jpg`,
            text: {
                title: '<h1>E-Commerce</h1>',
                p1: '<br>'+
                    '<p>'+
                        'Ter uma vitrine online onde o cliente possa ver os produtos desejados e os seus preços é maravilhoso. Mas deixar esses produtos acessíveis '+
                        'para serem comprados do conforto da sua residência é com certeza muito melhor.'+
                    '</p>',
                p2: '<br>'+
                    '<p>'+
                        'E não é apenas de lojas tradicionais que estamos falando, seja parafacilitar a contratação de um serviço, por exemplo, ou para a venda de '+
                        'produtos digitais, o E-commerce é a resposta ideal.'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                        'Esse serviço entretanto não se sustenta sozinho e está sempre associado a outros três serviços que a Brazilian Harpy também oferece: '+
                        '<a href="#" onClick="aClickService(`devWeb-Card`)"><strong>Desenvolvimento Web</strong></a>, <a href="#" onClick="aClickService(`appMobile-Card`)" ><strong>Aplicativos Mobile</strong></a> e '+
                        '<a href="#" onClick="aClickService(`appDesktop-Card`)" ><strong>Aplicativos Desktop.</strong></a> '+
                    '</p>',
                p4: '<br>'+
                    '<p>'+
                        'Isso pelo fato de que a loja virtual nada mais é que um banco de dados com todos os seus pro produtos e valores, mas é através do site '+
                        'ou aplicativo que o cliente terá de fato acesso a essas informações, podendo finalmente contratar/comprar o seu serviço/produto.'+
                    '</p>',
                p5: '',
                p6: '',
                button: 'Solicite a sua própria loja virtual'
            }
        },
        digitalMarketing: {
            photo: `${imagePlace}digitalMarketing.jpg`,
            text: {
                title: '<h1>Marketing Digital</h1>',
                p1: '<br>'+
                    '<p>Não importa o quão bom seja o seu negócio, se ele não conseguir alcançar novos clientes, vai fechar as portas.</p>',
                p2: '<br>'+
                    '<p>'+
                        'É pensando nisso que a Brazilian Harpy compreende que tão importante quanto te ajudar a ter um serviço de excelência, é te ajudar a '+
                        'ter um maor alcance para que novas pessoas conheçam esse serviço.'+
                    '</p>',
                p3: '<br>'+
                    '<p>'+
                        'Dessa forma fornecemos para você um serviçode Marketing Digital, impulsionando o seu website nas buscas do Google assim como as redes '+
                        'sociais da sua empresa para que a sua marca e o seu trabalho alcancem mais pessoas. Esse é o serviço perfeito para quem quer aumentar '+
                        'o raio de impacto que a sua empresa produz no mundo.'+
                    '</p>',
                p4: '',
                p5: '',
                p6: '',
                button: 'Expanda já o seu negócio'
            }
        }

    }
}