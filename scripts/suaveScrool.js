const HOME = document.querySelectorAll('.homeMenu');
const SERVICESS = document.querySelectorAll('.servicesMenu');
const WEB = document.querySelectorAll('.devWebMenu');
const APPS = document.querySelectorAll('.appsMenu');
const DESKTOP = document.querySelectorAll('.desktop-Menu');
const MARKETING = document.querySelectorAll('.marketingMenu');
const ABOUT = document.querySelectorAll('.aboutMenu');
const CONTACT = document.querySelectorAll('.contactMenu');
const verMais = document.querySelector('#btnMore');
let Language;

if(window.location.hash){
    if(window.location.hash == "#en-us") Language = "en-us"
    else Language = 'pt-br';
}




HOME.forEach(home => {
    home.addEventListener('click', event => {
        event.preventDefault();
        suaveScrool("#home")
    })
});

SERVICESS.forEach(services => {
    services.addEventListener('click', event => {
        event.preventDefault();
        suaveScrool("#services")
    })
});

WEB.forEach(services => {
    services.addEventListener('click', event => {
        event.preventDefault();
        suaveScrool("#services");
        let thecard = document.querySelector('#devWeb-Card');
        
        createNewServicePage(thecard, Language); 
    })
});

APPS.forEach(services => {
    services.addEventListener('click', event => {
        event.preventDefault();
        suaveScrool("#services");

        let thecard = document.querySelector('#appMobile-Card');
        
        createNewServicePage(thecard, Language);
    })
});

DESKTOP.forEach(services => {
    services.addEventListener('click', event => {
        event.preventDefault();
        suaveScrool("#services");

        let thecard = document.querySelector('#appDesktop-Card');
        
        createNewServicePage(thecard, Language);
    })
});

MARKETING.forEach(services => {
    services.addEventListener('click', event => {
        event.preventDefault();
        suaveScrool("#services");
        let thecard = document.querySelector('#digitalMarketing-Card');
        
        createNewServicePage(thecard, Language);
    })
});

ABOUT.forEach(about => {
    about.addEventListener('click', event => {
        event.preventDefault();
        suaveScrool("#about")
    })
});

CONTACT.forEach(contact => {
    contact.addEventListener('click', event => {
        event.preventDefault();
        suaveScrool("#contact");
    })
});

verMais.addEventListener('click', event => {
    suaveScrool("#services")
})






function suaveScrool(element){
    const Element = document.querySelector(element);
    Element.scrollIntoView( { behavior: "smooth" } );

}