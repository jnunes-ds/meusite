
// Show the languages options
function showLanguages(){
    let languages = document.getElementById("languagesFlags");

    if(languages.classList == "hide"){
        languages.classList.remove("hide");
    }else{
        languages.classList.add("hide");
    }
}

// define language reload anchors
var dataReload = document.querySelectorAll("[data-reload]");
var atualFlag = document.getElementById("atual-flag");
var lng;

class languagesClass{
    constructor(helloWorld, flag){
        this.helloWorld = helloWorld;
        this.flag = flag;
    }
}

// Language translations
var flagchange = {
    us: "./images/flags/us.jpg",
    br: "./images/flags/br.png"
}

var menuLanguages = {
    en: {
        home: 'Home',
        services: {
            title: 'Services',
            web: 'Web Dev.',
            apps: 'Applications',
            marketing: 'Digital Marketing'
        },
        about: 'About',
        contact: '<strong>Contact</strong>'
    },
    pt: {
        home: 'Início',
        services: {
            title: 'Serviços',
            web: 'Desen. Web',
            apps: 'Aplicativos',
            marketing: 'Marketing Digital'
        },
        about: 'Sobre',
        contact: '<strong>Contato</strong>'
    }
}

var languages = {
    en: {
        slide1Title: "Reach more <br>people!",
        slide1Text: "The Brazilian Harpy develops websites tailored to your needs, "+
                    "going <br>where social media don’t go and putting your business on "+
                    "<br>Google’s radar.",
        slide2Title: "Profit more, <br>work less!",
        slide2Text: "Stop the repetitive work and increase your productivity with the Brazilian Harpy’s <br> automatization "+
                    "program. Desktop applications for <br>performance improvement.",
        slide3Title: "More contact between you <br>and your costumers!",
        slide3Text: "Have a mobile application to mantain connection with the costumers <br>you already "+
                    "managed to attract.",
        aboutTitle: 'About',
        verMais: "View More", 
        aboutText: `
        <p>
            Initially created with the aim of providing solutions only in the area of ​​web 
            development, Brazilian Harpy presents today a range of complementary services 
            much more varied. These services are one hundred percent based on your demands 
            and needs.
        </p>

        <p>
            Our fundamental proposal is to provide the digital tools that your business needs, 
            be it a medium-sized company, small business, or even in cases of individual 
            entrepreneurs.
        </p>
        
        <p>
            These tools are diverse and developed according to your needs, such as:
        </p>

        <br>

        <ul>
            <li>
                The creation of websites, digital marketing services and the creation of online stores 
                in case your need is to expand your brand and reach more people;
            </li>

            <li>
                The creation of mobile and desktop applications in case your main problem is the time you 
                lose (or your employees) performing repetitive tasks that must be automated;
            </li>

            <li>
                Or even other services like creating apps for your customers (‘a very common request for 
                small restaurants that want to escape the delivery fees of traditional apps’).
            </li>
        </ul>
        
        <br>

        <p>
            And these are just a few applications of our services, as the possibilities are endless.
        </p>
        `,
        interSection1: `
                <p class="phrase">
                    "The dialogue creates the basis for collaboration."
                </p>
                <p class="author">
                    - Paulo Freire
                </p>
        `,
        interSection2: `
            <p class="phrase">
                "choose a job you love, and you will never have to work a Day in your life"
            </p>
            <p class="author">
                - Confucius
            </p>
        `,
        interSection3: `
            <p class="phrase">
                "What makes the boat go is not the full sail, but the wind that is not seen."
            </p>
            <p class="author">
                - Plato
            </p>
        `,
        contactTitle: 'Contact'
    },
    pt: {
        slide1Title: "Alcance mais <br>pessoas!",
        slide1Text: "A Brazilian Harpy desenvolve websites sob medida para  a sua necessidade, "+
                    "indo <br>onde as redes sociais não alcançam e colocando o seu negócio "+ 
                    "no radar da <br>Google.",
        slide2Title: "Ganhe mais, <br>trabalhe menos!",
        slide2Text: "Pare com o trabalho repetitivo e aumente a produtividade com o <br>programa de "+
                    "automatização da Brazilian Harpy. Aplicações Desktop para <br>melhora de performace.",
        slide3Title: "Mais contato entre você <br>e seus clientes!",
        slide3Text: "Tenha um aplicativo mobile para manter a conexão com os clientes que <br>você já "+
        "conseguiu captar.",
        verMais: "Ver Mais",
        aboutTitle: 'Sobre',
        aboutText: `
        <p>
            Criada inicialmente com o intuito de fornecer soluções unicamente na área de 
            desenvolvimento web, a Brazilian Harpy apresenta hoje uma gama de serviços 
            complementares muito mais variados. Serviços esses, cem porcento baseados nas 
            suas demandas e necessidades. 
        </p>

        <p>
            Nossa proposta fundamental é fornecer as ferramentas digitais que o seu negócio 
            precisa, seja ele uma empresa de médio porte, pequeno porte, ou mesmo em casos 
            de trabalhadores individuais.
        </p>
        
        <p>
            Essas ferramentas  são diversas e desenvolvidas sob medida de acordo com as suas 
            necessidades, como por exemplo:
        </p>

        <br>

        <ul>
            <li>
                A criação de websites, serviços de marketing digital e criação de lojas online 
                caso a sua necessidade seja de expandir a sua marca e alcançar mais pessoas;
            </li>

            <li>
                A criação de palicações mobile e desktop caso seu principal problema seja o tempo 
                que você perde (Ou seus funcionários) executando tarefas repetitivas que devem ser 
                automatizadas;
            </li>

            <li>
                Ou mesmo outros serviços como criação de apps para os seus clientes (‘Pedido muito 
                comun para pequenos restaurantes que desejam fugir das taxas de entrega dos 
                aplicativos tradicionais’).
            </li>
        </ul>
        
        <br>

        <p>
            E essas são apenas algumas aplicações dos nossos serviços, pois as possibilidades 
            são infinitas.
        </p>
        `,
        interSection1: `
                <p class="phrase">
                    "O diálogo cria base para colaboração."
                </p>
                <p class="author">
                    - Paulo Freire
                </p>
        `,
        interSection2: `
            <p class="phrase">
                "Escolhe um trabalho de que gostes e não terás que trabalhar nem um dia na tua vida."
            </p>
            <p class="author">
                - Confúcio
            </p>
        `,
        interSection3: `
            <p class="phrase">
                "O que faz andar o barco não é a vela enfunada, mas o vento que não se vê."
            </p>
            <p class="author">
                - Platão
            </p>
        `,
        contactTitle: 'Contato'
        
    }
};

// Define language via window hash
function changeLanguage(){
    if(window.location.hash){
        if(window.location.hash === "#en-us") lng = languages.en;
        else lng = languages.pt;
    }
    
    
    changeFlag(location.hash);
    changeMenu(location.hash);
    changeText(lng);
}

// Define language reload
dataReload.forEach(flag => {
    flag.onclick = function(){
        refresh();  
    }
})

function refresh(){
    setTimeout(function(){
        window.location.reload();
    }, 100);
}
changeLanguage();
