const NORMALCARDS = document.querySelectorAll('.card');
const MOBILECARDS = document.querySelectorAll('.mobile-card')
let Hash = window.location.hash;


function catchCard(){
    let language;
    
    if(Hash === "#en-us"){
        language = LNG.EnUs;
    }else{
        language = LNG.PtBr;
    }
    
    NORMALCARDS.forEach(card => {
        changeCardslanguage(language,card);
    });

    MOBILECARDS.forEach(card => {
        changeCardslanguage(language,card);
    });
    
}

function changeCardslanguage(language, card){
    const TITLE = document.querySelector(`#${card.id} .card-title`);
    const content = document.querySelector(`#${card.id} .card-text-content`);
    const back = document.querySelector(`#${card.id} .card-content-back`);

    let local;
    
    if(card.id === 'devWeb-Card' 
    || card.id === 'devWeb-mobile-Card'
    || card.id === 'first-Card-Clone'){
        local = language.devWeb;
    }else if(card.id === 'appMobile-Card' 
    || card.id === 'appMobile-mobile-Card'){
        local = language.appMobile;
    }else if(card.id === 'appDesktop-Card' 
    || card.id === 'appDesktop-mobile-Card'){
        local = language.appDesktop;
    }else if(card.id === 'marketPlace-Card'
    || card.id === 'marketPlace-mobile-Card'){
        local = language.marketPlace;
    }else if(card.id === 'digitalMarketing-Card'
    || card.id === 'digitalMarketing-mobile-Card'
    || card.id === 'last-Card-Clone'){
        local = language.digitalMarketing;
    }

    
    TITLE.innerHTML = local.title;
    content.innerHTML = local.content;
    back.innerHTML = local.back;
}


const LNG = {
    EnUs: {
        devWeb: {
            title: 'Web Development',
            content: `
                    <p>
                        The website is a mandatory tool for more than 99% of business and this should be obvious.
                    </p>
                    <p>
                        Wether to expand the number of costumers by facilitating Google’s search engines
                        (<a href="#Services"><strong>See also on Digital Marketing</strong></a>), or simply 
                        to keep a positioning of your company so that those who are already customers can 
                        learn more, the website...
                    </p>
                    `,
            back: 'Web Development'
        },
        appMobile: {
            title: 'Mobile Apps',
            content: `
                    <p>
                        The computer model most used in the world today is the smartphone
                    </p>
                    <p>
                        In this way, any self-respecting company (whether large or small), knows that mastering this tecnology 
                        is not longer a differentiator, but an obligation
                    </p>
                    <p>
                        Whether to have control of your business in the palm of you hand, to ...
                    </p>
                   `,
            back: 'Mobile <br>Applications'
        },
        appDesktop: {
            title: 'Desktop Apps',
            content: `
                    <p>
                        Whether to automate tasks or to have greater control over your company’s processes, desktop 
                        applications are excellent.   
                    </p>
                    <p>
                        These programs (or applications) are tailor-made by Brazilian Harpy in order to facilitate or, 
                        if possible, automate activities that although essential for the functioning of your business,  
                        are repetitive and end...
                    </p>
                    `,
            back: 'Desktop <br>Applications'
        },
        marketPlace: {
            title: 'E-commerce',
            content: `
                    <p>
                        Having an online showcase where customers can see the desired products and their prices is 
                        wonderful. But making these products accessible to be bought from the confort of you home is 
                        certainly much better.
                    </p>
                    <p>
                        And it’s not just about traditional stores that we’re talking, whether it’s to facilitate the ...
                    </p>
                    `,
            back: 'E-commerce'
        },
        digitalMarketing: {
            title: 'Digital Marketing',
            content: `
                    <p>
                        No matter how good your business is, if it fails to reach new customers, it will close its doors.
                    </p>
                    <p>
                        It is with this in mind that Brazilian Harpy understands that as important as helping you to have 
                        an excellent service, is helping you to have a greater reach so that new people get to know your 
                        service.
                    </p>
                    <p>
                    This way we provide...
                    </p>
                    `,
            back: 'Digital Marketing'
        }
    },
    PtBr: {
        // devWeb: {
        //     title: 'Desenvolvimento Web',
        //     content: `
        //             <p>
        //                 O website é uma ferramenta obrigatória para mais de 99% dos negócios e isso deveria ser óbvio.
        //             </p>
        //             <p>
        //                 Seja para expandir o número de clientes por facilitar os mecanismos de busca do Google
        //                 (<a href="#Services"><strong>veja também sobre Marketing Digital</strong></a>), ou simplesmente
        //                 para manter um posicionamento...
        //             </p>
        //             `,
        //     back: ''
        // },
        appMobile: {
            title: 'Aplicativos Mobile',
            content: `
                    <p>
                        O modelo de computador mais utilizado no mundo hoje é o smartphone.
                    </p>
                    <p>
                        Dessa forma, qualquer empresa que se preze (Seja de grande ou pequeno 
                        porte), sabe que dominar essa tecnologia não é mais um diferencial, 
                        mas uma obrigação.
                    </p>
                    <p>
                        Seja para ter o controle do seu negócio na palma da sua mão, para ...
                    </p>
                    `,
            back: 'Aplicativos <br>Mobile'
        },
        appDesktop: {
            title: 'Aplicativos Desktop',
            content: `
                    <p>
                        Seja para automatizar tarefas ou ter maior controle sobre os processos da 
                        sua empresa, as aplicações desktop são excelentes.   
                    </p>
                    <p>
                        Esses programas (ou aplicativos) são feitos sob medida pela Brazilian Harpy 
                        afim de facilitar ou, se possível, automatizar atividades que apesar de 
                        essenciais para o funcionamento do seu negócio, são repetitivas ...
                    </p>
                    `,
            back: 'Aplicativos <br>Desktop'
        },
        marketPlace: {
            title: 'E-commerce',
            content: `
                    <p>
                        Ter uma vitrine online onde o cliente possa ver os produtos desejados e 
                        os seus preços é maravilhoso. Mas deixar esses produtos acessíveis para 
                        serem comprados do conforto da sua residência é com certeza muito melhor.
                    </p>
                    <p>
                        E não é apenas de lojas tradicionais que estamos falando, seja para 
                        facilitar ...
                    </p>
                    `,
            back: 'E-commerce'
        },
        digitalMarketing: {
            title: 'Marketing Digital',
            content: `
                    <p>
                        Não importa o quão bom seja o seu negócio, se ele não conseguir 
                        alcançar novos clientes, vai fechar as portas.
                    </p>
                    <p>
                        É pensando nisso que a Brazilian Harpy compreende que tão importante
                        quanto te ajudar a ter um serviço de excelência, é te ajudar a ter um 
                        maor alcance para que novas pessoas conheçam esse serviço...
                    </p>
                    `,
            back: 'Marketing Digital'
        }
    },
}

catchCard()    
