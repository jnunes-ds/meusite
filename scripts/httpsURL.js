makeItSafe();

function makeItSafe(){
    if(location.protocol !== 'https:'){
        const preURL = location.href.split('//')[1];
        const URL = 'https://' + preURL;

        location.replace(URL);
    }
}