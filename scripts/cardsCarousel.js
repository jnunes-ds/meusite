const CAROUSEL = document.querySelector('#carousel');
const CARDSSLIDE =  document.querySelectorAll('.mobile-card');

// Buttons
const prevButton = document.querySelector('#prevCardBtn');
const nextButton = document.querySelector('#nextCardBtn');

// Counter
let counter = 1;


// const cardSize = 250;
// const totalMargin = 100;


let size = 275;

// if(window.innerWidth <= 400 && window.innerWidth > 350){
//     size = 250;
// }else if(window.innerWidth <= 350 && window.innerWidth > 300){
//     size = 275;
// }else if(window.innerWidth <= 300){
//     size = 255;
// }

CAROUSEL.style.transform = `translateX(${-size * counter}px)`;




nextButton.addEventListener('click', () => {
    if(counter >= (CARDSSLIDE.length - 1)) return;
    CAROUSEL.style.transition = "transform 0.4s ease-in-out";
    counter++;
    CAROUSEL.style.transform = `translateX(${-size * counter}px)`;
    flipTheCard();

});

prevButton.addEventListener('click', () => {
    if(counter <= 0) return;
    CAROUSEL.style.transition = "transform 0.4s ease-in-out";
    counter--;
    CAROUSEL.style.transform = `translateX(${-size * counter}px)`;
    flipTheCard();


});

CAROUSEL.addEventListener('transitionend', () => {
    if(CARDSSLIDE[counter].id === 'last-Card-Clone'){
        CAROUSEL.style.transition = "none";
        counter = CARDSSLIDE.length - 2;
        hideTheCard();
        CAROUSEL.style.transform = `translateX(${-size * counter}px)`;
        flipTheCard();
    }else if(CARDSSLIDE[counter].id === 'first-Card-Clone'){
        CAROUSEL.style.transition = "none";
        counter = CARDSSLIDE.length - counter;
        hideTheCard();
        CAROUSEL.style.transform = `translateX(${-size * counter}px)`;
        flipTheCard();
    }
})

function flipTheCard(){

    setTimeout(function(){  
        CARDSSLIDE.forEach(item => {
            item.classList.remove('flip');
        })
        CARDSSLIDE[counter].classList.add('flip')
        CARDSSLIDE[counter].classList.add('show-the-card');

    }, 1500)
}

const hideTheCard = () => {
    CARDSSLIDE.forEach(item => {
        item.classList.remove('show-the-card')
    })
}

flipTheCard();

