const MENU = document.querySelector('#side-menu');
const menuBtn = document.querySelector('.menu-btn');
const LI1 = document.querySelector('#Li1');
const servicesLabel = document.querySelector('#servicesMenu');
const servicesMenu = document.querySelector('#submenuMobile1');
let menuOpen = false;
let sub1Open = false;


// Open and Close principal Menu

menuBtn.addEventListener('click', () => {
    if(!menuOpen){
        menuBtn.classList.add('open');
        openMobileMenu();
        menuOpen = true;
    }else{
        menuBtn.classList.remove('open');
        closeSubMenu1();
        closeMobileMenu();
        menuOpen = false;
    }
});

function openMobileMenu(){
    MENU.style.width = "300px";
}

function closeMobileMenu(){
    MENU.style.width = "0px";
}


// Open and close Services Menu

servicesLabel.addEventListener('click', () => {
    if(!sub1Open){
        openSubMenu1();
    }else{
        closeSubMenu1();
    }
})

function openSubMenu1(){
    servicesMenu.style.height = "260px";
    sub1Open = true;
}

function closeSubMenu1(){
    servicesMenu.style.height = "0px";
    sub1Open = false;

}
 

console.log()