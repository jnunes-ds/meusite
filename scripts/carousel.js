const carouselContainer = document.querySelector('.carousel-slide ul');
const carouselImages = document.querySelectorAll('.carousel-slide ul li');
const totalSlides = carouselImages.length;



const NEXTBTN = document.getElementById('nextBtn');
const PREVBTN = document.getElementById('prevBtn');


let slidePosition = 0;

NEXTBTN.addEventListener('click', () => {
    moveToNextSlide()
})

PREVBTN.addEventListener('click', () => {
    moveToPreviousSlide()
});

// setInterval(() => {
//     moveToNextSlide();
// }, 5000);   


function updateSlidePosition(){
    carouselImages.forEach(slide => {
        slide.classList.remove('visible');
        slide.classList.add('invisible');
    })
    setTimeout(function(){
        carouselImages.forEach(slide => {
            slide.classList.add('hide')
        })
        carouselImages[slidePosition].classList.remove('invisible'); 
        carouselImages[slidePosition].classList.add('visible'); 
        carouselImages[slidePosition].classList.remove('hide'); 
    }, 500);
    
}


function moveToNextSlide(){
    if(slidePosition === totalSlides -1){
        slidePosition = 0;
    }else{
        slidePosition++
    }
    updateSlidePosition()
}

function moveToPreviousSlide(){
    if(slidePosition === 0){
        slidePosition = (totalSlides - 1);
    }else{
        slidePosition--;
    }
    updateSlidePosition()
}

setInterval(moveToNextSlide, 5500);