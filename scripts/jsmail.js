const buttonSend = document.querySelector('#enviar')

buttonSend.addEventListener('click', () => {
    console.log('enviarEmail')
    enviarEmail()
})


function enviarEmail() {
    let template = {    
        to_name: "Júnior Nunes",
        reply_to: document.querySelector('#emailInput').value,
        message: document.querySelector('#ContactMessage').value,
        from_phone: document.querySelector('#phoneInput').value,
        from_firstname: document.querySelector('#firstName').value,
        from_lastname: document.querySelector('#lastName')  .value

    };

    let expReg = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
    let testMail = expReg.test(template.reply_to);

    if(template.from_firstname != '' && template.from_lastname != '' &&
        template.reply_to != '' && template.message != ''){
            if(!testMail){
                (window.location.hash === '#en-us')
                ? alert('Attention, fill in with a valid email')
                : alert('Atenção, preencha com um e-mail válido');
            }else{
                emailjs.send("service_s5hbbse","template_yxuqwhl", template)
                    .then(res => {
                        if(res.status >= 200 && res.status < 300){
                            (window.location.hash === '#en-us')
                            ? alert('Email successfully sent!')
                            : alert('Email enviado com sucesso!');


                            document.querySelector('#firstName').value = '';
                            document.querySelector('#lastName').value = '';
                            document.querySelector('#phoneInput').value = '';
                            document.querySelector('#emailInput').value = '';
                            document.querySelector('#ContactMessage').value = '';
                        }else{
                            if(window.location.hash === '#en-us'){
                                alert('Alerta! Seu e-mail não foi encaminhado.'+
                                'Tente novamente ou encaminhe um e-mail diretamente'+
                                'para: contact@brharpy.com')
                            }else{
                                alert(`
                                    Alert! Your email has not been forwarded.
                                    Try again or forward an email directly to:
                                    contact@brharpy.com
                                `)
                            }
                        }
                    })
            }
        }else{
            (window.location.hash === '#en-us')
            ? alert('Heads up! ALL MANDATORY fields must be filled')
            : alert('Atenção! TODOS os campos OBRIGATÓRIOS devem ser preenchidos')
        }

}