const firstName = document.querySelector('#firstName');
const lastName = document.querySelector('#lastName');
const email = document.querySelector('#emailInput');
const phone = document.querySelector('#phoneInput');
const message = document.querySelector('#ContactMessage');
const btn = document.querySelector('#enviar');


function changeContacLanguage(){
    let lng;

    if(window.location.hash === '#en-us') lng = contactLanguages.EnUs;
    else lng = contactLanguages.PtBr;

    firstName.placeholder = lng.firstName;
    lastName.placeholder = lng.lastName;
    email.placeholder = lng.email;
    phone.placeholder = lng.phone;
    message.placeholder = lng.msg;
    btn.value = lng.send;
}

const contactLanguages = {
    EnUs: {
        firstName: '*First Name',
        lastName: '*Last Name',
        email: 'Email',
        phone: '*Phone: +1 555-555-555 (Optional)',
        msg: '  * Type your message here ...',
        send: 'Submit'
    },
    PtBr: {
        firstName: '*Nome',
        lastName: '*Sobrenome',
        email: '*E-mail',
        phone: 'Telefone: +55(11)99999-9999 (Opcional)',
        msg: '  * Digite aqui a sua mensagem...',
        send: 'Enviar'
    },
}

changeContacLanguage();